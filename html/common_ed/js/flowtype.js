/*
* FlowType.JS v1.1
* Copyright 2013-2014, Simple Focus http://simplefocus.com/
*
* FlowType.JS by Simple Focus (http://simplefocus.com/)
* is licensed under the MIT License. Read a copy of the
* license in the LICENSE.txt file or at
* http://choosealicense.com/licenses/mit
*
* Thanks to Giovanni Difeterici (http://www.gdifeterici.com/)
*/

(function($) {
   $.fn.flowtype = function(options) {

   var isThis = new function() {
      this.ua = navigator.userAgent;

      this.iPhone = this.ua.indexOf("iPhone") != -1;
      this.iPad = this.ua.indexOf("iPad") != -1;
      this.iOS = this.iPhone || this.iPad;

      this.Android = this.ua.indexOf("Android") != -1;
      this.Android2 = this.ua.indexOf("Android 2") != -1;
      this.Android4 = this.ua.indexOf("Android 4") != -1;
      this.AndroidSmartPhone = this.Android && this.ua.indexOf("Mobile") != -1 && this.ua.indexOf("A1_07") == -1 && this.ua.indexOf("SC-01C") == -1;
      this.AndroidTablet = this.Android && (this.ua.indexOf("Mobile") == -1 || this.ua.indexOf("A1_07") != -1 || this.ua.indexOf("SC-01C") != -1);

      this.sp = this.iPhone || this.AndroidSmartPhone;
      this.tablet = this.iPad || this.AndroidTablet;
   };


// Establish default settings/variables
// ====================================
      var settings = $.extend({
         maximum   : 9999,
         minimum   : 1,
         maxFont   : 9999,
         minFont   : 1,
         fontRatio : 35
      }, options),

// Do the magic math
// =================
      changes = function(el) {
         var $el = $(el),
            elw = $el.width(),
            width = elw > settings.maximum ? settings.maximum : elw < settings.minimum ? settings.minimum : elw,
            fontBase = width / settings.fontRatio,
            fontSize = fontBase > settings.maxFont ? settings.maxFont : fontBase < settings.minFont ? settings.minFont : fontBase;

         $el.css('font-size', fontSize + 'px');
      };

// Make the magic visible
// ======================
      return this.each(function() {
      // Context for resize callback
         var that = this;
      // Make changes upon resize
          if (!isThis.tablet) {
            $(window).on('resize', function(){changes(that);});
         }
      // Set changes on load
         changes(this);
      });
   };
}(jQuery));