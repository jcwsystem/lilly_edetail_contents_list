///////////////////////////////////////////////////////////////////////////////////
　　//////////////////////////// 　　文字挿入　　　////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

// ブランド略称
// NS: 	str / cym
// MSK: cym / frt
// ONC: alm / ram => lilly
// ONC: abe
// DMG: tlc / gly / hlg /hmt / trz / jad / baq / lum => lilly
// AI: tal / olm
var PRODUCT = 'tal';

// イベントID（URLから取得）
// yymmdd + ブランド略称
var EVENTID = '1234olm999';

// GAタグ用prefix / AUTO
var PREFIX = 'veeva_vod' + EVENTID;

//「電子添文」のリンク先　（未入力の場合は、電子添文ボタンが非表示になる）
// var DOCUMENT = 'https://www.lillymedical.jp/jp/ja/ra/olumiant/index.aspx';
var DOCUMENT = 'https://test.com';

//  EQ MID - Normal
var MOVIEVOD = '431';

//  EQ MID - High Speed
var HSMOVIEVOD = '411';


///////////////////////////////////////////////////////////////////////////////////
/////
///// 自動生成
/////

// ロゴパス / AUTO
if (PRODUCT == 'alm' || PRODUCT == 'ram' || PRODUCT == 'tlc' || PRODUCT == 'gly' || PRODUCT == 'hlg' || PRODUCT == 'hmt' || PRODUCT == 'trz' || PRODUCT == 'jad' || PRODUCT == 'baq' || PRODUCT == 'lum'){
  var LOGOPC = '<img src="../../../common_ed/images/logo_lilly_small.png" alt="">';
} else {
  var LOGOPC = '<img src="../../../common_ed/images/logo_' + PRODUCT + '.png" alt="">';
}

// // フッター共通
var FOOTERLOGO = '<img src="../../../common_ed/images/logo_lilly.png" alt="Lilly"/>';
var FLASHBANNER = '<a href="http://get.adobe.com/jp/flashplayer/" target="_blank" onclick="ga(\'send\', \'event\', \'Outbound\', \'Click\', \'http://get.adobe.com/jp/flashplayer/_' + '_' + EVENTID + '\');" class="tacosLogEvent" data-eventcd="flash"><img src="../../../common_ed/images/flash.png" alt=""/></a>';
var FLASHBANNERTXT = '<img src="../../../common_ed/images/flash_text.png" alt=""/>';
var COPYRIGHT = 'Copyright &copy; Eli Lilly Japan K.K. All rights reserved.';

$(function() {
	$('#brandLogo').html(LOGOPC);
	$('#eqPlayer').attr('data-normal-speed-mid', MOVIEVOD);

	// 倍速再生が必要な場合のみ表示
	$('#speedChange').hide();
	if (HSMOVIEVOD != ''){
		$('#speedChange').show();
    $('#eqPlayer').attr('data-high-speed-mid', HSMOVIEVOD);
	};

	// 電子添文がある場合のみ表示
	$('#document').hide();
	if (DOCUMENT != ''){
		$('#document').html('<a href="' + DOCUMENT + '" target="_blank" class="tacosLogEvent" data-eventcd="document" onClick="ga(\'send\', \'event\', \'Outbound\', \'Click\', \'' + DOCUMENT + '_' + '_' + EVENTID + '\');">電子添文情報を見る</a>');
		$('#document').show();
	};

  // DMG領域のみフッターロゴの表示非表示
  // ONC: alm / ram
  // DMG: tlc / gly / hlg /hmt / trz / jad
  if (PRODUCT == 'alm' || PRODUCT == 'ram' || PRODUCT == 'tlc' || PRODUCT == 'gly' || PRODUCT == 'hlg' || PRODUCT == 'hmt' || PRODUCT == 'trz' || PRODUCT == 'jad' || PRODUCT == 'baq' || PRODUCT == 'lum'){
    $('#footerLogo').hide();
  }

  // フッター共通
  if(document.URL.match('movie')) {
    $('#footerLogo').html(FOOTERLOGO);
  }
  $('#flashBanner').html(FLASHBANNER);
  $('#flashText').html(FLASHBANNERTXT);
  $('#copyright').html(COPYRIGHT);

});


// // Google Analytics
// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
// (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
// m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

// ga('create', 'UA-69864277-7', 'auto'); //IDを更新
// ga('send', 'pageview');



///////////////////////////////////////////////////////////////////////////////////
/////
///// viewport.js
/////

(function() {

	var isThis = new function() {
		this.ua = navigator.userAgent;

		this.iPhone = this.ua.indexOf("iPhone") != -1;
		this.iPad = this.ua.indexOf("iPad") != -1;
		this.iOS = this.iPhone || this.iPad;

		this.Android = this.ua.indexOf("Android") != -1;
		this.Android2 = this.ua.indexOf("Android 2") != -1;
		this.Android4 = this.ua.indexOf("Android 4") != -1;
		this.AndroidSmartPhone = this.Android && this.ua.indexOf("Mobile") != -1 && this.ua.indexOf("A1_07") == -1 && this.ua.indexOf("SC-01C") == -1;
		this.AndroidTablet = this.Android && (this.ua.indexOf("Mobile") == -1 || this.ua.indexOf("A1_07") != -1 || this.ua.indexOf("SC-01C") != -1);

		this.sp = this.iPhone || this.AndroidSmartPhone;
		this.tablet = this.iPad || this.AndroidTablet;
	};

	var isThisPc = new function() {
		this.ua = navigator.userAgent;

		this.windows = this.ua.indexOf("Windows NT") != -1;
		this.mac = this.ua.indexOf("Macintosh") != -1;

		this.ie11 = this.ua.indexOf("Trident/7") != -1;
		this.edge = this.ua.indexOf("Edge") != -1;
		this.firefox = this.ua.indexOf("Firefox") != -1;
		this.chrome = this.ua.indexOf("Chrome") != -1 && this.ua.indexOf("Edge") == -1;
		this.safari = this.ua.indexOf("Safari") != -1 && this.ua.indexOf("Chrome") == -1;
	};

	if(document.URL.match('movie')) {
		// ONC: alm / ram
		// DMG: tlc / gly / hlg /hmt / trz / jad / baq / lum
		if (PRODUCT == 'alm' || PRODUCT == 'ram' || PRODUCT == 'tlc' || PRODUCT == 'gly' || PRODUCT == 'hlg' || PRODUCT == 'hmt' || PRODUCT == 'trz' || PRODUCT == 'jad' || PRODUCT == 'baq' || PRODUCT == 'lum'){
			document.write('<link rel="stylesheet" type="text/css" href="../../../common_ed/brand/lilly/css/color.css">');
		} else {
			document.write('<link rel="stylesheet" type="text/css" href="../../../common_ed/brand/' + PRODUCT + '/css/color.css">');
		}

		window.isThis = isThis;
		window.isThisPc = isThisPc;
	}
})();


if(document.URL.match('movie')) {

	///////////////////////////////////////////////////////////////////////////////////
	/////
	///// index.js
	/////

	(function() {
		var player;

		var mediaInfo = {
			speedMode: 'normal',
			currentTime: 0,
			normalSpeedModeText: '通常の速度に戻す<br>（1.0倍）',
			highSpeedModeText: '倍速再生で視聴する<br>（1.3倍）',
			highSpeedRate: 1.3,
			rewindTime: 1
		};

		$(function() {
			$('#speedChange').html(mediaInfo.highSpeedModeText).addClass('is-normal');;
			mediaInfo.mid = $('#eqPlayer').attr('data-normal-speed-mid');
			mediaInfo.hsMid = $('#eqPlayer').attr('data-high-speed-mid');

			setPlayer(mediaInfo.mid, 0);
			setSpeedChangeClickEvent();
		});

		function setPlayer(mid, startTime, autoplay) {
			var ip = autoplay ? "on" : "off";

			// player = jstream_t3.PlayerFactoryIF.create({
			// 	b: "eqb673ulpm.eq.webcdn.stream.ne.jp/www50/eqb673ulpm/jmc_pub/jmc_swf/player/",
			// 	c: "MjA1Mg==",
			// 	m: mid,
			// 	t: startTime,
			// 	s: {
			// 		dq: "0",
			// 		el: "off",
			// 		rp: 'fit',
			// 		sn: "",
			// 		// hp: height,
			// 		// wp: width,
			// 		tg: "off",
			// 		ti: "off",
			// 		ip: ip

			// 		// el:"off",
			// 		// hp:450,
			// 		// il:"off",
			// 		// ip:"on",
			// 		// sn:"",
			// 		// tg:"off",
			// 		// ti:"off",
			// 		// wp:800
			// 	}
			// }, "eqPlayer");
			player = jstream_t3.PlayerFactoryIF.create({
				b: "eqc393ggdp.eq.webcdn.stream.ne.jp/www50/eqc393ggdp/jmc_pub/jmc_swf/player/",
				c: "Mjc3Nw==",
				m: mid,
				t: startTime,
				s:{
					el: "off",
					fs: "off",
					// hp:360,
					il: "off",
					skb: "off",
					sn: "",
					tg: "off",
					// wp:640
					rp: "fit",
					ip: ip,
				}}, "eqPlayer");

			EQP.control({ mid: mid, eid: EVENTID }, player);
		}

		function setSpeedChangeClickEvent() {
			$('#speedChange').on('click', function() {
				var playerState = player.accessor.state;
				if (playerState !== 'landing' && playerState !== 'playing' &&
					playerState !== 'paused' && playerState !== 'complete') {
					return false;
				}

				mediaInfo.currentState = playerState;

				if (mediaInfo.speedMode === 'normal') {
					mediaInfo.currentTime = player.accessor.getCurrentTime();
					mediaInfo.speedMode = 'highSpeed';
				} else {
					mediaInfo.currentTime = player.accessor.getCurrentTime() * mediaInfo.highSpeedRate;
					mediaInfo.speedMode = 'normal';
				}
				changeSpeed();
				return false;
			});
		}

		function changeSpeed() {
			var mid, text, startTime, className;
			if (mediaInfo.speedMode === 'normal') {
				mid = mediaInfo.mid;
				text = mediaInfo.highSpeedModeText;
				startTime = mediaInfo.currentTime - mediaInfo.rewindTime;
				className = mediaInfo.speedMode;
			} else {
				mid = mediaInfo.hsMid;
				text = mediaInfo.normalSpeedModeText;
				startTime = (mediaInfo.currentTime - mediaInfo.rewindTime) / mediaInfo.highSpeedRate;
				className = mediaInfo.speedMode;
			}

			if (startTime < 0) {
				startTime = 0;
			}

			$('#speedChange').html(text).removeClass().addClass('is-' + className);

			var autoplay = mediaInfo.currentState === 'playing' && (isThisPc.windows && (isThisPc.edge || isThisPc.firefox || isThisPc.ie11) ? true : false);

			setPlayer(mid, startTime, autoplay);
		}
	})();

}
